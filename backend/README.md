<h1 align="center">Backend</h1>


## Requisitos:
- Java 11
- Postgresql

## Configuração:
Com o java e o postgres instalados, configure o arquivo [application.properties](https://gitlab.com/Lucas-Severo/desafio-az/-/blob/master/backend/src/main/resources/application.properties)

``` java
spring.datasource.url=jdbc:postgresql://localhost:5432/db_name
spring.datasource.username=username
spring.datasource.password=password
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.PostgreSQLDialect
```

Com arquivo [application.properties](https://gitlab.com/Lucas-Severo/desafio-az/-/blob/master/backend/src/main/resources/application.properties) configurado, crie um banco de dados com o mesmo nome informado na url

## Instalação:
Execute os comandos a seguir de acordo com o sistema operacional:
### Windows:
```mvnw.cmd clean install```

### Linux:
```./mvnw clean install```

## Execução:
Com o comando acima gerará uma pasta chamada target, dessa forma entre na pasta e execute o jar gerado:
```bash
cd target/
java -jar backend-1.0.jar
```

