package com.desafio.az.domain.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desafio.az.domain.model.Automovel;
import com.desafio.az.domain.repository.AutomovelRepository;

@Service
public class AutomovelService {

	@Autowired
	private AutomovelRepository automovelRepository;
	
	public List<Automovel> listar() {
		return automovelRepository.findAll();
	}
	
	public Automovel cadastrar(Automovel automovel) {
		automovel.setDataCadastro(getDate());
		automovel.setDataAlterado(getDate());
		
		return automovelRepository.save(automovel);
	}
	
	public Automovel atualizar(Long id, Automovel automovel) {
		Automovel automovelExists = automovelRepository.findById(id)
			.orElseThrow(() -> new RuntimeException("Automóvel não encontrado"));
		
		if(!automovelExists.isVendido()) {
			automovel.setId(id);
			automovel.setDataCadastro(automovelExists.getDataCadastro());
			automovel.setDataAlterado(getDate());
			return automovelRepository.save(automovel);
		}
		
		throw new RuntimeException("Este automóvel já foi vendido, não pode ser alterado");
	}
	
	public void remover(Long id) {
		Optional<Automovel> automovel = automovelRepository.findById(id);
		
		if(automovel.isPresent()) {
			if(!automovel.get().isVendido()) {
				automovelRepository.deleteById(id);
				return;
			}
			throw new RuntimeException("Automóvel já vendido");
		}
		
		throw new RuntimeException("Automóvel não encontrado");
	}
	
	public void vender(Long id) {
		Automovel automovel = automovelRepository.findById(id)
				.orElseThrow(() -> new RuntimeException("Automovel não encontrado"));
		
		if(automovel.isVendido()) {
			throw new RuntimeException("Já vendido");
		}
		
		automovel.setVendido(true);
		automovelRepository.save(automovel);

	}
	
	public List<Automovel> listarVendidos(boolean condition) {
		return automovelRepository.findAllByVendido(condition);
	}
	
	private String getDate() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		LocalDateTime date = LocalDateTime.now();
		
		return date.format(formatter);
	}
	
}
