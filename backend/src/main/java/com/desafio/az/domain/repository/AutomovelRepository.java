package com.desafio.az.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.desafio.az.domain.model.Automovel;

public interface AutomovelRepository extends JpaRepository<Automovel, Long>{

	List<Automovel> findAllByVendido(boolean vendido);
	
}
