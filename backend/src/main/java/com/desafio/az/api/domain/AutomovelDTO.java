package com.desafio.az.api.domain;

public class AutomovelDTO {

	private Long id;
	
	private String marca;
	
	private String modelo;
	
	private String valorVenda;
	
	private String dataCadastro;
	
	private String dataAlterado;
	
	private boolean vendido;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getValorVenda() {
		return valorVenda;
	}

	public void setValorVenda(String valorVenda) {
		this.valorVenda = valorVenda;
	}

	public String getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(String dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getDataAlterado() {
		return dataAlterado;
	}

	public void setDataAlterado(String dataAlterado) {
		this.dataAlterado = dataAlterado;
	}

	public boolean isVendido() {
		return vendido;
	}

	public void setVendido(boolean vendido) {
		this.vendido = vendido;
	}
	
}
