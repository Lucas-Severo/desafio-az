package com.desafio.az.api.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.desafio.az.api.domain.AutomovelDTO;
import com.desafio.az.domain.model.Automovel;
import com.desafio.az.domain.service.AutomovelService;

@RestController
@RequestMapping("/automovel")
@CrossOrigin
public class AutomovelController {

	@Autowired
	private AutomovelService automovelService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public AutomovelDTO cadastrarAutomovel(@RequestBody AutomovelDTO automovel) {
		return toModel(automovelService.cadastrar(toEntity(automovel)));
	}
	
	@GetMapping
	public List<AutomovelDTO> listarAutomoveis() {
		return toCollectionModel(automovelService.listar());
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<AutomovelDTO> atualizarAutomovel(@PathVariable Long id, @RequestBody AutomovelDTO automovel) {
		return ResponseEntity.ok(toModel(automovelService.atualizar(id, toEntity(automovel))));
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> removerAutomovel(@PathVariable Long id) {
		automovelService.remover(id);
		return ResponseEntity.noContent().build();
	}
	
	@GetMapping("/vender/{id}")
	public ResponseEntity<Void> venderAutomovel(@PathVariable Long id) {
		automovelService.vender(id);
		return ResponseEntity.noContent().build();
	}
	
	// Método para listar os automóveis através
	// da condição se ele foi ou não vendido
	@GetMapping("/vendidos")
	public List<AutomovelDTO> listarVendidos(@RequestParam("vendido") boolean condicao) {
		return toCollectionModel(automovelService.listarVendidos(condicao));
	}
	
	// map
	
	private Automovel toEntity(AutomovelDTO automovel) {
		return modelMapper.map(automovel, Automovel.class);
	}
	
	private List<AutomovelDTO> toCollectionModel(List<Automovel> automoveis) {
		return automoveis.stream()
				.map(automovel -> {
					return toModel(automovel);
				})
				.collect(Collectors.toList());
	}
	
	private AutomovelDTO toModel(Automovel automovel) {
		return modelMapper.map(automovel, AutomovelDTO.class);
	}
	
}
