CREATE TABLE Automovel (

	id bigint NOT NULL primary key,
	marca varchar(100) NOT NULL,
	modelo varchar(100) NOT NULL,
	valorVenda decimal(19, 2) NOT NULL

);
