<h1 align="center">Desafio</h1>


## Clone:
Baixe este projeto com o comando:
```git clone https://gitlab.com/Lucas-Severo/desafio-az.git```

Após isso entre na pasta:
```cd desafio-az```

## Configuração:
Após isso, configure o ambiente

### Configurando o Backend:
```cd backend/```

[Backend](backend/README.md)

### Configurando o Frontend:
```cd frontend/```

[Frontend](frontend/README.md)
