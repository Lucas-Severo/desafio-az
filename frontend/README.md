<h1 align="center">Frontend</h1>


## Requisitos:
- Nodejs
- NPM

## Configuração:
```npm install```

## Execução:
```npm run serve -- --port 8081```

***O backend deve estar executando na porta 8080***

Com isso, abra no seu navegador [http://localhost:8081](http://localhost:8081)
